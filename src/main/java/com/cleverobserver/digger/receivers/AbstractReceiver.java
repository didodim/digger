package com.cleverobserver.digger.receivers;

/**
 *  Abstract Receiver - extends it for adding new Receivers
 * Created by Velihan on 08/09/2015.
 */

import org.apache.spark.storage.StorageLevel;
import org.apache.spark.streaming.receiver.Receiver;

public abstract class AbstractReceiver<T> extends Receiver<T> {

    protected Integer RESTART_AFTER = 20000;
    protected StorageLevel storageLevel;

    public AbstractReceiver(StorageLevel storageLevel) {

        super(storageLevel);
        this.storageLevel = storageLevel;
    }

    public AbstractReceiver() {
        this(StorageLevel.MEMORY_AND_DISK_2());
    }

    @Override
    public void onStart() {

        Thread t = new Thread(() -> receive());
        t.start();

    }

    protected abstract void receive();

    @Override
    public void onStop() {
    }


    @Override
    public StorageLevel storageLevel() {
        return storageLevel;
    }

}
