package com.cleverobserver.digger.models;

import java.util.EnumMap;

/**
 * Created by Velihan on 08/09/2015.
 */
public class Car extends EnumMap<CarProperties,String> {

    public Car() {
        super(CarProperties.class);
    }
}
